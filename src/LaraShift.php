<?php
namespace LaraShift;


/**
 * Class LaraShift
 *
 * @package LaraShift
 */
class LaraShift
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @param Config $config
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }
}