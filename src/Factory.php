<?php

namespace LaraShift;

use LaraShift\IO\IOInterface;
use Symfony\Component\Console\Formatter\OutputFormatterStyle;

/**
 * Class Factory
 *
 * @package LaraShift
 */
class Factory
{

    /**
     * @param  IOInterface|null $io
     *
     * @return Config
     */
    public static function createConfig(IOInterface $io = null, $cwd = null)
    {
        $cwd = $cwd ?: getcwd();
        $config = new Config($cwd);
        // determine and add main dirs to the config
        $config->merge(array(
            'config' => array(
                'home' => '',
                'cache-dir' => '',
                'data-dir' => '',
            ),
        ));

        return $config;
    }


    /**
     * @return array
     */
    public static function createAdditionalStyles()
    {
        return array(
            'highlight' => new OutputFormatterStyle('red'),
            'warning' => new OutputFormatterStyle('black', 'yellow'),
        );
    }


    /**
     * @param IOInterface $io
     * @param null        $localConfig
     * @param null        $cwd
     * @param bool|true   $fullLoad
     *
     * @return LaraShift
     */
    public function createLaraShift(
        IOInterface $io,
        $localConfig = null,
        $cwd = null,
        $fullLoad = true
    ) {
        $cwd = $cwd ?: getcwd();
        // Load config and override with local config/auth config
        $config = static::createConfig($io, $cwd);
        if (!is_null($localConfig)) {
            $config->merge($localConfig);
        }
        // initialize
        $laraShift = new LaraShift();
        $laraShift->setConfig($config);
        if ($fullLoad) {
            // load auth configs into the IO instance
            $io->loadConfiguration($config);
        }
        return $laraShift;
    }

    /**
     * @param IOInterface $io
     * @param null        $config
     *
     * @return mixed
     */
    public static function create(IOInterface $io, $config = null)
    {
        $factory = new static();
        return $factory->createLaraShift($io, $config);
    }
}