<?php

namespace LaraShift\Console;

use Symfony\Component\Console\Application as BaseApplication;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Console\Formatter\OutputFormatter;
use LaraShift\IO\ConsoleIO;
use LaraShift\Factory;
//commands
use LaraShift\Console\Commands\ValidateCommand;
use LaraShift\Console\Commands\UpgradeCommand;

class Application extends BaseApplication
{
    const NAME = 'LaraShift Console Application';
    const VERSION = '1.0.0@dev-master';
    protected $laraShift;
    /**
     * @var IOInterface
     */
    protected $io;

    public function __construct()
    {
        parent::__construct(static::NAME, static::VERSION);
    }

    public function getLaraShift()
    {
        if (null === $this->laraShift) {
            try {
                $this->laraShift = Factory::create($this->io, null);
            } catch (\InvalidArgumentException $e) {
                $this->io->writeError($e->getMessage());
            }
        }

        return $this->laraShift;
    }

    /**
     * {@inheritDoc}
     */
    public function run(
        InputInterface $input = null,
        OutputInterface $output = null
    ) {
        if (null === $output) {
            $styles    = Factory::createAdditionalStyles();
            $formatter = new OutputFormatter(null, $styles);
            $output    = new ConsoleOutput(ConsoleOutput::VERBOSITY_NORMAL,
                null,
                $formatter);
        }

        return parent::run($input, $output);
    }

    /**
     * {@inheritDoc}
     */
    public function doRun(InputInterface $input, OutputInterface $output)
    {
        $this->io = new ConsoleIO($input, $output, $this->getHelperSet());

        return parent::doRun($input, $output);
    }

    /**
     * Removes the cached larashift instance
     */
    public function resetLaraShift()
    {
        $this->laraShift = null;
    }

    /**
     * Gets the default commands that should always be available.
     *
     * @return array An array of default Command instances
     */
    protected function getDefaultCommands()
    {
        // Keep the core default commands to have the HelpCommand
        // which is used when using the --help option
        $defaultCommands = parent::getDefaultCommands();

        $defaultCommands[] = new ValidateCommand();
        $defaultCommands[] = new UpgradeCommand();

        return $defaultCommands;
    }

    /**
     * @return IOInterface
     */
    public function getIO()
    {
        return $this->io;
    }

}