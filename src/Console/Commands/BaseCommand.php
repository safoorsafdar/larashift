<?php
namespace LaraShift\Console\Commands;

use LaraShift\LaraShift;
use Symfony\Component\Console\Command\Command;
use LaraShift\Console\Application;
use LaraShift\IO\IOInterface;
use LaraShift\IO\NullIO;

class BaseCommand extends Command
{

    /**
     * @var LaraShift
     */
    private $laraShift;
    /**
     * @var IOInterface
     */
    private $io;

    public function getLaraShift()
    {
        if (null === $this->laraShift) {
            $application = $this->getApplication();
            if ($application instanceof Application) {
                /* @var $application    Application */
                $this->laraShift = $application->getLaraShift();
            } else {
                throw new \RuntimeException(
                    'Could not create a LaraShift\LaraShift instance, you must inject '
                    .
                    'one if this command is not used with a LaraShift\Console\Application instance'
                );
            }
        }

        return $this->laraShift;
    }

    public function setLaraShift(LaraShift $laraShift)
    {
        $this->laraShift = $laraShift;
    }

    public function resetComposer()
    {
        $this->laraShift = null;
        $this->getApplication()->resetLaraShift();
    }

    /**
     * @return IOInterface
     */
    public function getIO()
    {
        if (null === $this->io) {
            $application = $this->getApplication();
            if ($application instanceof Application) {
                /* @var $application    Application */
                $this->io = $application->getIO();
            } else {
                $this->io = new NullIO();
            }
        }

        return $this->io;
    }

    /**
     * @param IOInterface $io
     */
    public function setIO(IOInterface $io)
    {
        $this->io = $io;
    }
}