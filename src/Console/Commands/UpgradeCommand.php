<?php
namespace LaraShift\Console\Commands;

use LaraShift\Console\Commands\BaseCommand;
use LaraShift\Action;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UpgradeCommand extends BaseCommand
{


    protected function configure()
    {
        $this
            ->setName('upgrade')
            ->setDescription('Upgrade the Laravel.')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Who do you want to greet?'
            )
            ->addOption(
                'dry-run',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io                    = $this->getIO();
        $dummyRequirementArray = [
            'to'      => "5.1.0",
            'from'    => "5.0.0",
            "actions" => [
                [
                    'what'  => "mkdir",
                    'param' => [
                        'name' => "cache",
                        'in'   => "bootstrap",
                    ],
                ],
                /* [
                     'what'  => "touch",
                     'param' => [
                         'name' => ".gitignore",
                         'in'   => "bootstrap/cache/",
                     ],
                 ],
                 [
                     'what'  => "echo",
                     'param' => [
                         'content' => ".gitignore",
                         'in'      => "bootstrap/cache/.gitignore",
                     ],
                 ],*/
            ],
        ];
        //resolve version requirement first
        //resolve
        foreach ($dummyRequirementArray['actions'] as $aAction) {
            $sAction = Action::NewInstance();
            $sAction->setRaw($aAction);
            $sAction->setIO($io);
            $sAction->setConfig($this->getLaraShift()->getConfig());
            if ($sAction->isValid()) {
                $this->getIO()->write('valid');
                //$sAction->handle();
            } else {
                $this->getIO()->writeError('in valid');
            }
        }
    }

}