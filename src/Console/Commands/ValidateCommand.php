<?php
namespace LaraShift\Console\Commands;

use LaraShift\Console\Commands\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class ValidateCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('validate')
            ->setDescription('Validate the current Laravela')
            ->addOption(
                'yell',
                null,
                InputOption::VALUE_NONE,
                'If set, the task will yell in uppercase letters'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $exclude               = ['vendor', 'node_modules', 'storage'];
        $directory             = $this->getLaraShift()->getConfig()
            ->getBaseDir();



    }
}