<?php
namespace LaraShift\Operation;


/**
 * Class TouchOperation
 *
 * @package LaraShift\Operation
 */
class TouchOperation extends Operation
{
    /**
     * @var
     */
    protected $param;

    /**
     * @param $sParam
     */
    function __construct($sParam)
    {
        $this->param = $sParam;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return true;
    }

    /**
     *
     */
    public function handle()
    {

    }
}