<?php

namespace LaraShift\Operation;


/**
 * Class EchoOperation
 *
 * @package LaraShift\Operation
 */
class EchoOperation extends Operation
{
    /**
     * @var
     */
    protected $param;

    /**
     * @param $sParam
     */
    function __construct($sParam)
    {
        $this->param = $sParam;
    }

    /**
     * @return bool
     */
    public function validate()
    {
        return true;
    }

    /**
     *
     */
    public function handle()
    {

    }
}