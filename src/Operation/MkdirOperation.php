<?php
namespace LaraShift\Operation;

use Symfony\Component\Filesystem\Filesystem;
use LaraShift\IO\IOInterface;
use LaraShift\Config;

/**
 * Class MkdirOperation
 *
 * @package LaraShift\Operation
 */
class MkdirOperation extends Operation
{
    /**
     * @var Filesystem
     */
    protected $oFileSystem;
    /**
     * @var IOInterface
     */
    protected $oIO;
    /**
     * @var Config
     */
    protected $oConfig;

    /**
     * @param                 $aParam
     * @param IOInterface     $oIO
     * @param Config          $oConfig
     * @param Filesystem|null $oFileSystem
     */
    function __construct(
        $aParam,
        IOInterface $oIO,
        Config $oConfig,
        Filesystem $oFileSystem = null
    ) {
        $this->aParam      = $aParam;
        $this->oIO         = $oIO;
        $this->oConfig     = $oConfig;
        $this->oFileSystem = $oFileSystem ?: new Filesystem();

    }

    /**
     * @return bool
     */
    public function validate()
    {
        $path = $this->oConfig->getBaseDir()
            .$this->aParam['in'].DIRECTORY_SEPARATOR.$this->aParam['name']
            .DIRECTORY_SEPARATOR;
        $this->oIO->comment($path);
        if ($this->oFileSystem->isAbsolutePath($path)) {
            return $this->oFileSystem->exists($path);
        }

        return false;
    }

    /**
     *
     */
    public function handle()
    {

    }
}