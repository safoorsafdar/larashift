<?php

namespace LaraShift\Operation;


/**
 * Class Operation
 *
 * @package LaraShift
 */
abstract class Operation
{
    /**
     * @var
     */
    protected $aParam;

    /**
     * @return mixed
     */
    abstract public function validate();

    /**
     * @return mixed
     */
    abstract public function handle();
}