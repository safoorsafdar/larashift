<?php
namespace LaraShift;


/**
 * Class Config
 *
 * @package LaraShift
 */
class Config
{
    /**
     * @var array
     */
    public static $defaultConfig
        = array(
            "exclude_dir" => [
                'vendor',
            ],
        );
    /**
     * @var array
     */
    private $config;
    /**
     * @var null
     */
    private $baseDir;

    /**
     * @param null $baseDir
     */
    public function __construct($baseDir = null)
    {
        $this->config  = static::$defaultConfig;
        $this->baseDir = $baseDir;
    }

    /**
     * @return null|string
     */
    public function getBaseDir(){
        return $this->realpath();
    }

    /**
     * @param int $flags
     *
     * @return array
     */
    public function all($flags = 0)
    {
        $all = array();
        foreach (array_keys($this->config) as $key) {
            $all['config'][$key] = $this->get($key, $flags);
        }

        return $all;
    }

    /**
     * @return array
     */
    public function raw()
    {
        return array(
            'config' => $this->config,
        );
    }


    /**
     * @param $key
     *
     * @return bool
     */
    public function has($key)
    {
        return array_key_exists($key, $this->config);
    }

    /**
     * @param $key
     *
     * @return mixed|null
     */
    public function get($key)
    {

        if ( ! isset($this->config[$key])) {
            return null;
        }

        return $this->process($this->config[$key]);
    }


    /**
     * @param $value
     *
     * @return mixed
     */
    private
    function process(
        $value
    ) {
        $config = $this;
        if ( ! is_string($value)) {
            return $value;
        }

        return preg_replace_callback('#\{\$(.+)\}#',
            function ($match) use ($config) {
                return $config->get($match[1]);
            }, $value);
    }

    /**
     * @param null $path
     *
     * @return null|string
     */
    private function realpath($path = null)
    {
        if (preg_match('{^(?:/|[a-z]:|[a-z0-9.]+://)}i', $path)) {
            return $path;
        }

        return $this->baseDir.'/'.$path;
    }

    /**
     * @param $config
     */
    public function merge($config)
    {
        // override defaults with given config
        if ( ! empty($config['config']) && is_array($config['config'])) {
            foreach ($config['config'] as $key => $val) {
                if (in_array($key, array(
                        'bitbucket-oauth',
                        'github-oauth',
                        'gitlab-oauth',
                        'http-basic',
                    ))
                    && isset($this->config[$key])
                ) {
                    $this->config[$key] = array_merge($this->config[$key],
                        $val);
                } elseif ('preferred-install' === $key
                    && isset($this->config[$key])
                ) {
                    if (is_array($val) || is_array($this->config[$key])) {
                        if (is_string($val)) {
                            $val = array('*' => $val);
                        }
                        if (is_string($this->config[$key])) {
                            $this->config[$key]
                                = array('*' => $this->config[$key]);
                        }
                        $this->config[$key] = array_merge($this->config[$key],
                            $val);
                        // the full match pattern needs to be last
                        if (isset($this->config[$key]['*'])) {
                            $wildcard = $this->config[$key]['*'];
                            unset($this->config[$key]['*']);
                            $this->config[$key]['*'] = $wildcard;
                        }
                    } else {
                        $this->config[$key] = $val;
                    }
                } else {
                    $this->config[$key] = $val;
                }
            }
        }
    }

}