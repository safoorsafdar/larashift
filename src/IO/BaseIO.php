<?php


namespace LaraShift\IO;

use LaraShift\Config;


/**
 * Class BaseIO
 *
 * @package LaraShift\IO
 */
abstract class BaseIO implements IOInterface
{

    /**
     * {@inheritDoc}
     */
    public function loadConfiguration(Config $config)
    {
        $bitbucketOauth = $config->get('bitbucket-oauth') ?: array();
        $githubOauth    = $config->get('github-oauth') ?: array();
        $gitlabOauth    = $config->get('gitlab-oauth') ?: array();
        $httpBasic      = $config->get('http-basic') ?: array();

    }
}
