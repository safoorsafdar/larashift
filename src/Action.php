<?php
namespace LaraShift;

use LaraShift\IO\IOInterface;
use LaraShift\Config;

/**
 * Class Action
 *
 * @package LaraShift
 */
class Action
{

    /**
     * @var null
     */
    private $aRaw;
    /**
     * @var null
     */
    private $oIO;
    /**
     * @var null
     */
    private $oConfig;


    /**
     *
     */
    function __construct()
    {
        $this->aRaw    = null;
        $this->oIO     = null;
        $this->oConfig = null;
    }

    /**
     * @return Action
     */
    public static function NewInstance()
    {
        return new self();
    }

    /**
     * @return mixed
     */
    public function validate()
    {
        $class    = ucfirst($this->what())."Operation";
        $resolver = __NAMESPACE__.'\\Operation\\'.$class;
        if ( ! class_exists($resolver)) {
            throw new \RuntimeException('Invalid working class specified, '
                .$resolver.' does not exist.');
        }

        return (new $resolver($this->param(), $this->getIO(),
            $this->getConfig()))->validate();
    }

    /**
     *
     */
    public function handle()
    {

    }

    /**
     * @param $aRaw
     *
     * @return $this
     */
    public function setRaw($aRaw)
    {
        $this->aRaw = $aRaw;

        return $this;
    }

    /**
     * @return null
     */
    public function getRaw()
    {
        return $this->aRaw;
    }

    /**
     * @param IOInterface $oIO
     *
     * @return $this
     */
    public function setIO(IOInterface $oIO)
    {
        $this->oIO = $oIO;

        return $this;
    }

    /**
     * @return null
     */
    public function getIO()
    {
        return $this->oIO;
    }

    /**
     * @param \LaraShift\Config $oConfig
     *
     * @return $this
     */
    public function setConfig(Config $oConfig)
    {
        $this->oConfig = $oConfig;

        return $this;
    }

    /**
     * @return null
     */
    public function getConfig()
    {
        return $this->oConfig;
    }

    /**
     * @return mixed
     */
    public function isValid()
    {
        return $this->validate();

    }

    /**
     * @return null
     */
    public function what()
    {
        return isset($this->aRaw['what']) ? $this->aRaw['what'] : null;
    }

    /**
     * @return null
     */
    public function param()
    {
        return isset($this->aRaw['param']) ? $this->aRaw['param'] : null;
    }


}